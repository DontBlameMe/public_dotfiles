#!/usr/bin/env sh

# Check if the required dependencies are installed
REQUIREMENTS="cd echo exit yt-dlp tr mkdir cat md2html"
"$SCRIPTS"/check_dependency -s $REQUIREMENTS || exit 0

# Ensure script is run from its directory
cd "$(dirname "$0")" || exit 1

VIDEO=$1

if [ -z "$VIDEO" ]; then
    echo "Usage: summarize_video <Youtube Video URL>"
    exit 1
fi

# Check if the URL is a valid Youtube Video URL
yt-dlp --simulate --skip-download "$VIDEO" > /dev/null 2>&1

if [[ $? != 0 ]]; then
    echo "Invalid Video URL"
    exit 1
fi

ORIGINAL_TITLE=$(yt-dlp --get-title "$VIDEO")
REPLACED_TITLE=$(echo "$ORIGINAL_TITLE" | tr '[:upper:]' '[:lower:]' | tr ' ' '_')
VIDEO_ID=$(yt-dlp --get-id "$VIDEO")
SUMMARY_PATH="summaries/${REPLACED_TITLE}_${VIDEO_ID}"
THUMBNAIL=$(yt-dlp --get-thumbnail "$VIDEO")

if [ -d "$SUMMARY_PATH" ]; then
    if [ ! -f "$SUMMARY_PATH/${REPLACED_TITLE}.html" ]; then
        rm -rf "$SUMMARY_PATH"
    else
        echo "Summary already exists" "Opening existing summary"
        $BROWSER "$SUMMARY_PATH/${REPLACED_TITLE}.html"
        exit 0
    fi
fi

clean_and_exit() {
    ./clean_up $SUMMARY_PATH $REPLACED_TITLE
    echo "An error occured: $1"
    exit 1
}

mkdir -p "$SUMMARY_PATH"

./download_subtitles $VIDEO $SUMMARY_PATH $REPLACED_TITLE || clean_and_exit "Failed to download subtitles"
./convert_subtitles_to_text $SUMMARY_PATH $REPLACED_TITLE || clean_and_exit "Failed to convert subtitles to text"
RESULT=$(./summarize_subtitles $SUMMARY_PATH $REPLACED_TITLE) || clean_and_exit "Failed to summarize subtitles"

# Prepend information about the Video
echo "### [$ORIGINAL_TITLE]($VIDEO)
<img src="$THUMBNAIL" alt="Thumbnail" width="320" height="180">


" > "$SUMMARY_PATH/$REPLACED_TITLE.md"

echo "$RESULT" >> "$SUMMARY_PATH/$REPLACED_TITLE.md"

if [ ! -f "$SUMMARY_PATH/$REPLACED_TITLE.txt" ]; then
    echo "No subtitles found"
    exit 1
fi

## Append the transcript
echo "<details>
<summary>Transcript</summary>

$(cat "$SUMMARY_PATH/$REPLACED_TITLE.txt")

</details>
" >> "$SUMMARY_PATH/$REPLACED_TITLE.md"

md2html "$SUMMARY_PATH/$REPLACED_TITLE.md" > "$SUMMARY_PATH/$REPLACED_TITLE.html"
$BROWSER "$SUMMARY_PATH/$REPLACED_TITLE.html"

./clean_up $SUMMARY_PATH $REPLACED_TITLE
