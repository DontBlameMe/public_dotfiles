#!/bin/zsh

autoload -U colors && colors

source /home/user/.config/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /home/user/.config/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /home/user/.config/zsh/plugins/you-should-use.zsh

export YSU_MESSAGE_POSITION="after"

# Automatically ls after dir change
autoload -U add-zsh-hook

ls_after_cd() {
    lsd -lA --group-dirs=first
}

add-zsh-hook chpwd ls_after_cd
