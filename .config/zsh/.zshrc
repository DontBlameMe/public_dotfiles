#!/bin/zsh

export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state

. "$XDG_CONFIG_HOME"/zsh/global_env.zsh

# Start X server
[[ -z "$DISPLAY" ]] && [[ $(tty) = /dev/tty1 ]] && Hyprland

# Source files
. "$XDG_CONFIG_HOME"/zsh/hotkeys.zsh
. "$XDG_CONFIG_HOME"/zsh/options.zsh
. "$XDG_CONFIG_HOME"/zsh/plugins.zsh
. "$XDG_CONFIG_HOME"/zsh/aliases.zsh

# Path
path_add() {
    [[ -d "$1" && ! $PATH =~ $1 ]] && path+=($1)
}

path_add "$XDG_DATA_HOME"/cargo/bin
path_add "$HOME"/.local/bin
path_add $GOPATH/bin

export PATH

# Clear on launch
clear

eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/zen.toml)"
eval "$(zoxide init zsh)"
eval "$(atuin init zsh)"
