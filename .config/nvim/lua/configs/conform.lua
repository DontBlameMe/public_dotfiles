local options = {
  formatters_by_ft = {
    lua = { "stylua" },
    css = { "prettier" },
    html = { "prettier" },
    rust = { "rust-analyzer" },
    cpp = { "clang-format" },
    sh = { "beautysh" },
    bash = { "beautysh" },
    csh = { "beautysh" },
    zsh = { "beautysh" },
    ksh = { "beautysh" },
    python = { "black" },
  },

  format_on_save = {
    timeout_ms = 500,
    lsp_fallback = true,
  },
}

return options
